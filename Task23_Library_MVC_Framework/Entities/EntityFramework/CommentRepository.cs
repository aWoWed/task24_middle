﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.Context;
using Task23_Library_MVC_Framework.Entities.Abstract;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.Entities.EntityFramework
{
    public class CommentRepository : ICommentRepository
    {
        private readonly LibraryDbContext _appDbContext;

        public CommentRepository(LibraryDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Gets All Comments from Db
        /// </summary>
        public IQueryable<Comment> Get() => _appDbContext.Comments;

        /// <summary>
        /// Gets Async All Comments from Db
        /// </summary>
        public Task<IQueryable<Comment>> GetAsync() => Task.FromResult(_appDbContext.Comments.AsQueryable());

        /// <summary>
        /// Gets Comment by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>FormAnswer with current key</returns>
        public Comment GetByKey(Guid key) => _appDbContext.Comments.FirstOrDefault(comment => comment.Id == key);

        /// <summary>
        /// Gets Async Comment by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>FormAnswer with current key</returns>
        public async Task<Comment> GetByKeyAsync(Guid key) =>
            await _appDbContext.Comments.FirstOrDefaultAsync(comment => comment.Id == key);

        /// <summary>
        /// Gets Comment by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>FormAnswer with current name</returns>
        public IQueryable<Comment> GetByName(string name) => _appDbContext.Comments.Where(comment => comment.Name == name);

        /// <summary>
        /// Gets Async Comment by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>FormAnswer with current name</returns>
        public Task<IQueryable<Comment>> GetByNameAsync(string name) =>
            Task.FromResult(_appDbContext.Comments.Where(comment => comment.Name == name));

        /// <summary>
        /// Gets Comment by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>FormAnswer with contains text</returns>
        public IQueryable<Comment> GetByContainsText(string text) =>
            _appDbContext.Comments.Where(comment => comment.Text.Contains(text));

        /// <summary>
        /// Gets Async Comment by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>FormAnswer with contains text</returns>
        public Task<IQueryable<Comment>> GetByContainsTextAsync(string text) => Task.FromResult(_appDbContext.Comments.Where(comment => comment.Text.ToLower().Contains(text.ToLower())));

        /// <summary>
        /// Inserts or Updates Comment to Db
        /// </summary>
        /// <param name="entity"></param>
        public void InsertOrUpdate(Comment entity) => _appDbContext.Entry(entity).State =
            _appDbContext.Comments.FirstOrDefault(comment => comment.Id == entity.Id) == null ? EntityState.Added: EntityState.Modified;

        /// <summary>
        /// Deletes Comment with current key
        /// </summary>
        /// <param name="key"></param>
        public void DeleteByKey(Guid key) => _appDbContext.Comments.Remove(new Comment {Id = key});

        /// <summary>
        /// Deletes all Comments
        /// </summary>
        public void DeleteAll() => _appDbContext.Comments.RemoveRange(_appDbContext.Comments);
    }
}
