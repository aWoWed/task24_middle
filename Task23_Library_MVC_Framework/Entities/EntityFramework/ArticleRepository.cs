﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.Context;
using Task23_Library_MVC_Framework.Entities.Abstract;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.Entities.EntityFramework
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly LibraryDbContext _appDbContext;

        public ArticleRepository(LibraryDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Gets All Articles from Db
        /// </summary>
        public IQueryable<Article> Get() => _appDbContext.Articles;

        /// <summary>
        /// Gets Async All Articles from Db
        /// </summary>
        public Task<IQueryable<Article>> GetAsync() => Task.FromResult(_appDbContext.Articles.AsQueryable());

        /// <summary>
        /// Gets article by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Article with current key</returns>
        public Article GetByKey(Guid key) => _appDbContext.Articles.FirstOrDefault(article => article.Id == key);

        /// <summary>
        /// Gets Async article by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Article with current key</returns>
        public async Task<Article> GetByKeyAsync(Guid key) =>
            await _appDbContext.Articles.FirstOrDefaultAsync(article => article.Id == key);

        /// <summary>
        /// Gets article by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Article with current name</returns>
        public IQueryable<Article> GetByName(string name) =>
            _appDbContext.Articles.Where(article => article.Name == name);

        /// <summary>
        /// Gets Async article by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Article with current name</returns>
        public Task<IQueryable<Article>> GetByNameAsync(string name) =>
            Task.FromResult(_appDbContext.Articles.Where(article => article.Name == name));

        /// <summary>
        /// Gets article by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Article with contains text</returns>
        public IQueryable<Article> GetByContainsText(string text) =>
            _appDbContext.Articles.Where(article => article.Text.ToLower().Contains(text.ToLower()));

        /// <summary>
        /// Gets Async article by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Article with contains text</returns>
        public Task<IQueryable<Article>> GetByContainsTextAsync(string text) =>
            Task.FromResult(_appDbContext.Articles.Where(article => article.Text.ToLower().Contains(text.ToLower())));

        /// <summary>
        /// Inserts or Updates article to Db
        /// </summary>
        /// <param name="entity"></param>
        public void InsertOrUpdate(Article entity) => _appDbContext.Entry(entity).State =
            _appDbContext.Articles.FirstOrDefault(article => article.Id == entity.Id) == null ? EntityState.Added : EntityState.Modified;

        /// <summary>
        /// Deletes article with current key
        /// </summary>
        /// <param name="key"></param>
        public void DeleteByKey(Guid key) => _appDbContext.Articles.Remove(new Article {Id = key});

        /// <summary>
        /// Deletes all articles
        /// </summary>
        public void DeleteAll() => _appDbContext.Articles.RemoveRange(_appDbContext.Articles);
    }
}
