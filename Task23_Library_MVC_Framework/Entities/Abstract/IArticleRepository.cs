﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.Entities.Abstract
{
    public interface IArticleRepository : IRepository<Guid, Article>
    { }
}
