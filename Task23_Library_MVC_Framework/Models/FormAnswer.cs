﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task23_Library_MVC_Framework.Models
{
    public class FormAnswer : BaseModel
    {
        public string Genres { get; set; }
        public string ReadingPassion { get; set; }
    }
}
