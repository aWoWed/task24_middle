﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.Models;
using Task23_Library_MVC_Framework.UOfW;

namespace Task23_Library_MVC_Framework.Controllers
{
    public class GuestController : Controller
    {
        /// <summary>
        /// Loads Comments from Db
        /// </summary>
        /// <returns>Comments Page</returns>
        public ActionResult Index()
        {
            var unitOfWork = new UnitOfWork();
            return View(unitOfWork.CommentRepository.Get());
        }

        /// <summary>
        /// Adds comment to Db
        /// </summary>
        /// <param name="comment"></param>
        /// <returns>Add comment</returns>
        [HttpPost]
        public ActionResult AddComment(Comment comment)
        {
            if (string.IsNullOrEmpty(comment.Name))
                ModelState.AddModelError("Name", "Error! Please enter your name!");

            if (string.IsNullOrEmpty(comment.Text))
                ModelState.AddModelError("Text", "Error! Please enter your text!");

            var unitOfWork = new UnitOfWork();

            if (ModelState.IsValid)
            {
                unitOfWork.CommentRepository.InsertOrUpdate(comment);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View("Index", unitOfWork.CommentRepository.Get());
        }
    }
}
