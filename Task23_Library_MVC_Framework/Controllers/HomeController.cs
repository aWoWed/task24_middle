﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.Models;
using Task23_Library_MVC_Framework.UOfW;

namespace Task23_Library_MVC_Framework.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Loads Articles from Db
        /// </summary>
        /// <returns>Articles Page</returns>
        public ActionResult Index()
        {
            var unitOfWork = new UnitOfWork();
            return View(unitOfWork.ArticleRepository.Get());
        }
    }
}