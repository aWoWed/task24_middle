﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.Models;
using Task23_Library_MVC_Framework.UOfW;

namespace Task23_Library_MVC_Framework.Controllers
{
    public class FormController : Controller
    {
        /// <summary>
        /// Loads Form ViewModel
        /// </summary>
        /// <returns>Form ViewModel</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Adds FormAnswer To the db
        /// </summary>
        /// <param name="formAnswer"></param>
        /// <param name="genres"></param>
        /// <returns>FormAnswer Page</returns>
        [HttpPost]
        public ActionResult Result(FormAnswer formAnswer, IList<string> genres)
        {
            if (string.IsNullOrEmpty(formAnswer.Name))
                ModelState.AddModelError("Name","Please enter your name");

            if (string.IsNullOrEmpty(formAnswer.Genres) || genres == null)
                ModelState.AddModelError("Genres", "Select at least one of the genres");

            if (string.IsNullOrEmpty(formAnswer.ReadingPassion))
                ModelState.AddModelError("ReadingPassion", "Please choice your reading passion");

            var unitOfWork = new UnitOfWork();

            if (genres != null)
                foreach (var genre in genres)
                {
                    if (!formAnswer.Genres.Contains(genre))
                        formAnswer.Genres += " " + genre;
                }

            if (!ModelState.IsValid) return View("Index");
            unitOfWork.FormAnswerRepository.InsertOrUpdate(formAnswer);
            unitOfWork.Save();
            return RedirectToAction("Result");
        }

        /// <summary>
        /// Loads FormAnswer Page
        /// </summary>
        /// <returns>FormAnswer Page</returns>
        [HttpGet]
        public ActionResult Result()
        {
            var unitOfWork = new UnitOfWork();
            return View("Result", unitOfWork.FormAnswerRepository.Get());
        }
    }
}
