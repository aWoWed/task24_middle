﻿using System.Threading.Tasks;
using Task23_Library_MVC_Framework.Context;
using Task23_Library_MVC_Framework.Entities.Abstract;
using Task23_Library_MVC_Framework.Entities.EntityFramework;

namespace Task23_Library_MVC_Framework.UOfW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryDbContext _appDbContext;
        private readonly IArticleRepository _articleRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly IFormAnswerRepository _formAnswerRepository;

        public UnitOfWork()
        {
            _appDbContext = new LibraryDbContext();
        }

        public IArticleRepository ArticleRepository => _articleRepository ?? new ArticleRepository(_appDbContext);  

        public IFormAnswerRepository FormAnswerRepository =>
            _formAnswerRepository ?? new FormAnswerRepository(_appDbContext);

        public ICommentRepository CommentRepository => _commentRepository ?? new CommentRepository(_appDbContext);

        /// <summary>
        /// Save Changes in Db
        /// </summary>
        public void Save() => _appDbContext.SaveChanges();

        /// <summary>
        /// Save Changes Async in Db
        /// </summary>
        public async Task SaveAsync() => await _appDbContext.SaveChangesAsync();
    }
}
