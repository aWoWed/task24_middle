﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.Entities.Abstract;

namespace Task23_Library_MVC_Framework.UOfW
{
    public interface IUnitOfWork
    {
        ICommentRepository CommentRepository { get; }
        IFormAnswerRepository FormAnswerRepository { get; }
        IArticleRepository ArticleRepository { get; }

        /// <summary>
        /// Save Changes in Db
        /// </summary>
        void Save();

        /// <summary>
        /// Save Async Changes in Db
        /// </summary>
        Task SaveAsync();
    }
}
