﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.Context
{
    public class LibraryDbContext : DbContext
    {
        /// <summary>
        /// Database Library Context
        /// </summary>
        static LibraryDbContext() => Database.SetInitializer(new LibraryContextInitializer());

        public LibraryDbContext() : base("name=DefaultConnection")
        { }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<FormAnswer> FormAnswers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
